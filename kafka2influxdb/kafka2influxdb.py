import json
import logging
import uuid
from time import sleep, time

import influxdb_client
from config import config
from confluent_kafka import Consumer, KafkaError, KafkaException
from influxdb_client.client.exceptions import InfluxDBError
from influxdb_client.client.write_api import SYNCHRONOUS, WriteOptions
from influxdb_client.domain.write_precision import WritePrecision
from message import message_transformer

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def print_assignment(consumer, partitions):
    logger.info(f"Assignment: {partitions}")


class BatchingCallback:
    def __init__(self):
        self.batch_counter = 0
        self.expected_batches = 0
        self.point_counter = 0
        self.total_points = 0
        self.start_time = 0

    def success(self, conf: (str, str, str), data: str):
        self.batch_counter += 1
        self.point_counter += data.decode().count("\n") + 1
        logger.info(
            f"Written batch {self.batch_counter}/{self.expected_batches}, points {self.point_counter}/{self.total_points}={self.point_counter/self.total_points*100:.0f}% in {time()-self.start_time:.3f} seconds."
        )
        logger.debug(f"Written batch: {conf}")
        self.start_time = time()

    def error(self, conf: (str, str, str), data: str, exception: InfluxDBError):
        if (
            hasattr(exception, "message")
            and "partial write: points beyond retention policy dropped"
            in exception.message
        ):
            # The points beyond the retention policy are igonred, the rest of the batch is written successfully.
            # This is the desired behaviour and the success callback is called.
            logger.error(exception.message)
            self.success(conf, data)
        else:
            logger.error(f"Cannot write batch: {conf}, due: {exception}")

    def retry(self, conf: (str, str, str), data: str, exception: InfluxDBError):
        logger.warning(f"Retryable error occurs for batch: {conf}, retry: {exception}")

    def reset(self, total_points):
        self.batch_counter = 0
        self.expected_batches = (total_points - 1) // config.getint(
            "InfluxDB", "batch_size"
        ) + 1
        self.point_counter = 0
        self.total_points = total_points
        self.start_time = time()

    def is_written(self):
        # if self.batch_counter == self.expected_batches:
        if self.point_counter == self.total_points:
            return True
        elif (time() - self.start_time) > 30 and self.point_counter > 0:
            # TODO
            # This section should be removed in the future.
            # It is here to ignore incomplete writes that happen occassionally for reasons that still need to be understood...
            # Search for ERROR logs to see if the problem still occurs.
            # I believe it has been solved by increasing the database flush_interval...
            logger.error(
                "Taking too long to write the remaining points. Ignorning the rest and Moving on to another batch..."
            )
            return True
        else:
            return False


class Kafka2InfluxDB:
    def __init__(self):
        # Consumer configuration
        # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
        conf = {
            "group.id": config.get("Kafka", "group.id"),
            "client.id": config.get("Kafka", "client.id", fallback=str(uuid.uuid4())),
            "bootstrap.servers": config.get("Kafka", "bootstrap.servers"),
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": config.get("Kafka", "sasl.username"),
            "sasl.password": config.get("Kafka", "sasl.password"),
            "auto.offset.reset": "latest",
            "enable.auto.commit": "false",
        }

        # Create a Consumer instance.
        self.consumer = Consumer(conf)
        logger.info(f"{conf['group.id']=} {conf['client.id']=}")

        # Subscribe to the topics belonging to the project.
        # - base topic: project_id
        # - application specific topics: project_id.application_id
        # consumer.subscribe(["tud_gv_test", r"^tud_gv_test\..*"], on_assign=print_assignment)
        topics = config.get("Kafka", "topics").split(",")
        self.consumer.subscribe(topics, on_assign=print_assignment)
        logger.info(f"Subscribed to topics: {topics}")

        # InfluxDB client
        self.influx_client = influxdb_client.InfluxDBClient(
            url=config.get("InfluxDB", "url"),
            token=config.get("InfluxDB", "token"),
            org=config.get("InfluxDB", "org"),
            enable_gzip=True,
            debug=False,
        )

        self.callback = BatchingCallback()
        # write_options = WriteOptions(batch_size=config.getint("InfluxDB", "batch_size"))
        write_options = WriteOptions(
            batch_size=config.getint("InfluxDB", "batch_size"),
            flush_interval=config.getint("InfluxDB", "flush_interval"),
            jitter_interval=config.getint("InfluxDB", "jitter_interval"),
        )
        # TODO
        # With the flush interval too short, we sometimes observed incomplete batches being written.
        # This happend mainly in the cloud.
        # The following explanation and conclusion still needs to be verified!!!
        # I assume the main difference between the cloud and my laptop is the network performance.
        # My laptop on a VPN is closer to the database than a container running in AWS.
        # I belive we need to reserve enough time (flush_interval) for a complete batch...
        # For example, I choose flush_interval of 5 seconds, which corresponds to the Kafka timeout for consuming batches.
        # The InfluxDB writes happen always after the flush_interval, e.g. even for a smaller batch we wait for 5 seconds.
        # Consuming batches of up to 50000 messages in Kafka means that we design for 10000 messages per second.
        self.write_api = self.influx_client.write_api(
            write_options=write_options,
            success_callback=self.callback.success,
            error_callback=self.callback.error,
            retry_callback=self.callback.retry,
        )

    def __del__(self):
        self.consumer.close()
        self.write_api.close()
        self.influx_client.close()

    def consume_next(self):
        # Read messages from Kafka.
        while True:
            try:
                # Instead of using poll that returns messages one by one,
                # we use consume to receive a list of messages.
                msgs = self.consumer.consume(
                    num_messages=config.getint("Kafka", "num_messages"),
                    timeout=config.getfloat("Kafka", "timeout"),
                )
                logger.info(f"Received {len(msgs)} messages.")
                if msgs:
                    break

            except KafkaError as err:
                logger.error(err)
                continue

        # Loop over the batch of messages obtained from Kafka.
        # Individual messages will be deserialized.
        # Messages that cannot be deserialized will be ignored.
        points = []
        for msg in msgs:
            value = message_transformer.deserialize(msg)
            if not value:
                continue
            if not message_transformer.is_valid_project(value, msg.topic()):
                continue
            points += message_transformer.get_points(value)

        logger.info(
            f"Extracted {len(points)} InfluxDB points from {len(msgs)} Kafka messages."
        )
        # logger.debug(json.dumps(points, indent=4))
        if not len(points):
            return

        self.callback.reset(len(points))
        try:
            self.write_api.write(
                bucket=config.get("InfluxDB", "bucket"),
                org=config.get("InfluxDB", "org"),
                record=points,
                write_precision=WritePrecision.MS,
            )
        except Exception as err:
            logger.error(f"Unexpected {err=}, {type(err)=}")
            return

        # Wait until the points are written to the database and commit the offsets to Kafka.
        start_time = time()
        wait_counter = 0
        while True:
            sleep(1)
            wait_counter += 1
            if self.callback.is_written():
                self.consumer.commit(asynchronous=False)
                logger.info("Committed offsets to Kafka.")
                break
            if wait_counter < 60 and wait_counter % 10 == 0:
                # logger.info("Waiting for the InfluxDB success callback.")
                logger.info(
                    f"Waiting for the InfluxDB success callback for {time()-start_time:.0f} seconds."
                )
            elif wait_counter % 60 == 0:
                logger.warning(
                    f"Waiting for the InfluxDB success callback for {(time()-start_time)/60:.0f} minutes."
                )
            if time() - start_time > 3600:
                raise Exception("Could not write to InfluxDB for 1 hour...")


if __name__ == "__main__":
    kafka2influxdb = Kafka2InfluxDB()
    try:
        while True:
            kafka2influxdb.consume_next()
    except KeyboardInterrupt:
        logger.error("Aborted by user.")
    finally:
        kafka2influxdb.__del__()
