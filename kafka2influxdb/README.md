# Kafka to InfluxDB

An implementation of a Kafka consumer that receives Green Village messages and writes them in batches to InfluxDB can be found here.

```bash
docker build . -t gv-kafka2influxdb
docker run -v $(pwd)/kafka2influxdb.cfg:/kafka2influxdb.cfg gv-kafka2influxdb
```
