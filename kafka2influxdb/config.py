import configparser


class Config:
    __conf = None

    @staticmethod
    def config():
        if Config.__conf is None:
            Config.__conf = configparser.ConfigParser()
            # Read the config file from the local directory.
            Config.__conf.read("kafka2influxdb.cfg")
        return Config.__conf


config = Config.config()
