import base64
import json
import logging

import fastavro
import requests
from config import config
from confluent_kafka import Message
from confluent_kafka.error import SerializationError
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.schema_registry.error import SchemaRegistryError
from confluent_kafka.serialization import MessageField, SerializationContext

logger = logging.getLogger(__name__)


class TGVMessageTransformer:
    def __init__(self) -> None:
        # SchemaRegistryClient is needed to instantiate AvroDeserializer.
        conf = {
            "url": config.get("Kafka", "schema_registry_url"),
            "basic.auth.user.info": config.get("Kafka", "schema_registry_auth"),
        }
        schema_registry_client = SchemaRegistryClient(conf)

        # Get the schema from the Schema Registry.
        # The schema is obtained from the "tud_gv_test" topic. It is unique for all topics.
        r = requests.get(
            f"{config.get('Kafka', 'schema_registry_url')}/subjects/tud_gv_test-value/versions/latest",
            headers={
                "Authorization": f"Basic {base64.b64encode(config.get('Kafka', 'schema_registry_auth').encode()).decode()}"
            },
        )
        schema_str = r.json()["schema"]
        logger.debug(f"{schema_str=}")

        # AvroDeserializer and SerializationContext are needed to deserialize individual messages.
        self.deserializer = AvroDeserializer(schema_registry_client, schema_str)
        self.ser_context = SerializationContext("tud_gv_test", MessageField.VALUE)

    def deserialize(self, msg: Message) -> dict:
        try:
            return self.deserializer(msg.value(), self.ser_context)
        except (
            SerializationError,
            fastavro._read_common.SchemaResolutionError,
        ) as err:
            logger.error(err)
            return None
        except SchemaRegistryError as err:
            logger.error(err)
            return None

    def is_valid_project(self, value: dict, topic: str) -> bool:
        # Verify that project_id and the base topic are the same,
        # unless the message comes from the tud_gv_test topic (staging environment).
        base_topic = topic.split(".")[0]
        # Strip the "tud_gv_" prefix from the topic name.
        project_from_topic = base_topic.split("_", 2)[2]
        # return (base_topic == "tud_gv_test" or value["project_id"] == project_from_topic)
        if base_topic == "tud_gv_test" or value["project_id"] == project_from_topic:
            return True
        else:
            logger.warning(
                f"Incompatible project_id {value['project_id']} and topic {msg.topic()}."
            )
            return False

    def get_points(self, value: dict) -> dict:
        # Every measurement from the message sent in the required format
        # is stored in InfluxDB in the following way:
        # - project_id is used as the name of the measurement.
        # - Time is taken from the message field timestamp.
        # - Tags:
        #     - unit, type
        #     - project_id, device_id, application_id, measurement_id
        #     - project_description, device_description, application_description, measurement_description
        #     - device_manufacturer, device_type, device_serial
        #     - location_id, location_descrpition
        # - Fields:
        #     - value
        #     - latitude, longitude, altitude

        # Loop over all measurements from the message.
        # Every measurement will be stored individually as series in InfluxDB.
        points = []
        for measurement in value["measurements"]:
            measurement_type = measurement["value"].__class__.__name__
            point = {
                "measurement": value["project_id"],
                "time": value["timestamp"],
                "tags": {
                    "unit": measurement["unit"],
                    "type": measurement_type,
                    "project_id": value["project_id"],
                    "application_id": value["application_id"],
                    "device_id": value["device_id"],
                    "measurement_id": measurement["measurement_id"],
                    "project_description": value["project_description"],
                    "application_description": value["application_description"],
                    "device_description": value["device_description"],
                    "measurement_description": measurement["measurement_description"],
                    "device_manufacturer": value["device_manufacturer"],
                    "device_type": value["device_type"],
                    "device_serial": value["device_serial"],
                    "location_id": value["location_id"],
                    "location_description": value["location_description"],
                },
                "fields": {
                    f"value_{measurement_type}": measurement["value"]
                    if not measurement["value"] is None
                    else 0,
                    "latitude": value["latitude"],
                    "longitude": value["longitude"],
                    "altitude": value["altitude"],
                },
            }
            points.append(point)

        return points


message_transformer = TGVMessageTransformer()
