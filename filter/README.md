# Kafka filters

The messages from the Green Village projects can be filtered at the Kafka level and streamed into a new topic. In this way, people with access to the new topic can read only the filtered data.

The motivation for this example is to give people access to a subset of messages. Kafka allows defining ACLs at the topic level. However, more fine-grained access control to a subset of messages within a topic is not possible. This is solved by filtering the data and producing to a new topic.
