import json
import logging
import os
import uuid
import base64
import requests

from confluent_kafka import Consumer, Producer
from confluent_kafka import KafkaError, KafkaException
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.schema_registry.error import SchemaRegistryError
from confluent_kafka.serialization import SerializationContext, MessageField
from confluent_kafka.serialization import StringSerializer
from confluent_kafka.error import SerializationError

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.DEBUG
)
logger = logging.getLogger(__file__)

TOPIC_FROM = "tud_gv_officelab-climate"
TOPIC_TO = "tud_gv_test"


def filter(value):
    """Filter a message."""
    selected = False
    if value["application_id"] == "Weather":
        selected = True
    return selected


def acked(err, msg):
    global delivered_records
    """Delivery report handler called on
    successful or failed delivery of message
    """
    if err is not None:
        logging.error(f"Failed to deliver message: {err}")
    else:
        delivered_records += 1
        logging.info(
            "Produced record to topic {} partition [{}] @ offset {}".format(
                msg.topic(), msg.partition(), msg.offset()
            )
        )


if __name__ == "__main__":
    global delivered_records
    delivered_records = 0
    total_records = 0

    logging.info(f"Consuming from topic {TOPIC_FROM}")
    logging.info(f"Producing to topic {TOPIC_TO}")

    # SchemaRegistryClient will be needed to instantiate AvroDeserializer.
    conf = {"url": os.environ["SCHEMA_REGISTRY_URL"], "basic.auth.user.info": os.environ["SCHEMA_REGISTRY_AUTH"]}
    schema_registry_client = SchemaRegistryClient(conf)

    # Get the schema from the Schema Registry.
    # The schema is obtained from the "tud_gv_test" topic. It is unique for all topics.
    r = requests.get(
        f"{os.environ['SCHEMA_REGISTRY_URL']}/subjects/tud_gv_test-value/versions/latest",
        headers={
            "Authorization": f"Basic {base64.b64encode(os.environ['SCHEMA_REGISTRY_AUTH'].encode()).decode()}"
        },
    )
    logger.info(f"Schema Registry response code: {r.status_code}")
    logger.debug(f"Schema Registry response: {r.text}")
    # schema_id = r.json()['id']
    schema_str = r.json()["schema"]

    # AvroDeserializer and SerializationContext will be needed to deserialize
    # individual messages.
    deserializer = AvroDeserializer(schema_registry_client, schema_str)
    ser_context = SerializationContext("tud_gv_test", MessageField.VALUE)

    producer = Producer(
        {
            "bootstrap.servers": os.environ["BOOTSTRAP_SERVERS"],
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": os.environ["API_KEY"],
            "sasl.password": os.environ["API_SECRET"],
        }
    )
    consumer = Consumer(
        {
            "bootstrap.servers": os.environ["BOOTSTRAP_SERVERS"],
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": os.environ["API_KEY"],
            "sasl.password": os.environ["API_SECRET"],
            "group.id": os.environ["CONSUMER_GROUP"],
            "auto.offset.reset": "latest",
        }
    )

    consumer.subscribe([TOPIC_FROM])

    # Process messages
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                # No message available within timeout.
                # Initial message consumption may take up to
                # `session.timeout.ms` for the consumer group to
                # rebalance and start consuming
                logger.info("Waiting for message or event/error in poll()")
                continue

            if msg.error():
                # On error, break and do not commit any offset to Kafka.
                raise KafkaException(msg.error())
                break

            try:
                value = deserializer(msg.value(), ser_context)
            except SerializationError as e:
                # Ignore the messages that cannot be deserialized.
                logger.error("SerializationError occurred, the message will be ignored and the offset will be committed: {}".format(e))
                consumer.commit()
                continue

            total_records += 1
            logger.debug(
                "%s [%d] at offset %d"
                % (msg.topic(), msg.partition(), msg.offset())
            )

            if filter(value):
                logging.debug("Filter passed.")
                producer.produce(
                    topic=TOPIC_TO,
                    value=msg.value(),
                    on_delivery=acked,
                )
                producer.poll(0)


    except KeyboardInterrupt:
        pass
    finally:
        # Leave group and commit final offsets
        consumer.close()
        producer.flush()
        logging.info(
            f"{delivered_records} out of {total_records} messages were produced!"
        )
